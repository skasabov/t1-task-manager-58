package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataBase64SaveRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-base64";

    @NotNull
    private static final String DESCRIPTION = "Save data from base64 file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64SaveListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        request.setToken(getToken());
        domainEndpoint.saveDataBase64(request);
    }

}
