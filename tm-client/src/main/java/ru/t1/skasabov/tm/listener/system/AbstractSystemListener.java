package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

}
