package ru.t1.skasabov.tm.listener.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ProjectUpdateByIdRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-update-by-id";

    @NotNull
    private static final String DESCRIPTION = "Update project by id.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(id, name, description);
        request.setToken(getToken());
        projectEndpoint.updateProjectById(request);
    }

}
