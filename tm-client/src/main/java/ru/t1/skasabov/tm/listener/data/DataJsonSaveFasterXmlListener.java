package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataJsonSaveFasterXmlRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-json";

    @NotNull
    private static final String DESCRIPTION = "Save data to json file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataJsonSaveFasterXmlListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest();
        request.setToken(getToken());
        domainEndpoint.saveDataJsonFasterXml(request);
    }

}
