package ru.t1.skasabov.tm.taskmanager;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.skasabov.tm.configuration.ServerConfiguration;

public abstract class AbstractTest {

    @NotNull
    protected static ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @BeforeClass
    @SneakyThrows
    public static void init() {
        @NotNull final Liquibase liquibase = context.getBean(Liquibase.class);
        liquibase.dropAll();
        liquibase.update("schema");
    }

}
