package ru.t1.skasabov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p", ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p", ProjectDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOne(@NotNull final ProjectDTO model) {
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", model.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) return;
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", project.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        entityManager.remove(project);
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final ProjectDTO project = findOneByIndex(index);
        if (project == null) return;
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", project.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        entityManager.remove(project);
    }

    @Override
    public void removeAll() {
        @NotNull final List<ProjectDTO> projects = findAll();
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                    .setParameter("projectId", project.getId())
                    .getResultList();
            for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) return;
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", project.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        entityManager.remove(project);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) return;
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", project.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        entityManager.remove(project);
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<ProjectDTO> projects = findAll(userId);
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                    .setParameter("projectId", project.getId())
                    .getResultList();
            for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByCreated() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.created",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByStatus() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.status",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByName() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.name",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.created",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.status",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.name",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }
    
}
