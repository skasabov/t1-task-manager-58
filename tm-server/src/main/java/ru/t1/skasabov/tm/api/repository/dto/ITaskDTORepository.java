package ru.t1.skasabov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllSortByCreated();

    @NotNull
    List<TaskDTO> findAllSortByStatus();

    @NotNull
    List<TaskDTO> findAllSortByName();

    @NotNull
    List<TaskDTO> findAllSortByCreatedForUser(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllSortByStatusForUser(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllSortByNameForUser(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjects();

    @NotNull
    List<TaskDTO> findAllByProjects(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByProjects(@NotNull Collection<ProjectDTO> collection);

}
