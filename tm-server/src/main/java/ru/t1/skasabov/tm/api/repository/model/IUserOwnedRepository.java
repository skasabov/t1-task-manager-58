package ru.t1.skasabov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    Boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    long getSize(@NotNull String userId);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll(@NotNull String userId);

}
