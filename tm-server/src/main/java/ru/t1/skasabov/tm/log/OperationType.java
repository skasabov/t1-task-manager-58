package ru.t1.skasabov.tm.log;

public enum OperationType {

    INSERT,
    DELETE,
    UPDATE

}
