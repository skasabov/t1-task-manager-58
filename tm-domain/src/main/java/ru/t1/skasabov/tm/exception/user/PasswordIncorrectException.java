package ru.t1.skasabov.tm.exception.user;

public final class PasswordIncorrectException extends AbstractUserException {

    public PasswordIncorrectException() {
        super("Error! Incorrect password entered. Please try again...");
    }

}
