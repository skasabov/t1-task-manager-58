package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class UserRoleRegistryRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private Role role;

    public UserRoleRegistryRequest(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

}
